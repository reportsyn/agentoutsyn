﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;
using System.IO;
using System.Transactions;
using System.Diagnostics;

namespace SafeSynTools
{
    public partial class Form1 : Form
    {
        int out_count = Convert.ToInt32(ConfigurationManager.AppSettings["out_count"]);//获取实时同步的筆數
        int out_time = Convert.ToInt32(ConfigurationManager.AppSettings["out_time"]);//获取实时同步的执行间隔时间
        int out_waitX = Convert.ToInt32(ConfigurationManager.AppSettings["out_waitX"]);//获取实时同步的执行间隔倍數
        int win_count = Convert.ToInt32(ConfigurationManager.AppSettings["win_count"]);//获取实时同步的筆數
        int win_time = Convert.ToInt32(ConfigurationManager.AppSettings["win_time"]);//获取实时同步的执行间隔时间
        int win_waitX = Convert.ToInt32(ConfigurationManager.AppSettings["win_waitX"]);//获取实时同步的执行间隔倍數
        string reportConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_report"].ToString();//report
        string fufuzhuConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_fufuzhu"].ToString();//fufuzhu
        string sql_agentOut = ConfigurationManager.ConnectionStrings["sqlConnectionString_agentOut"].ToString();
        string sql_totalIn = ConfigurationManager.ConnectionStrings["sqlConnectionString_totalIn"].ToString();
        string sql_totalOut = ConfigurationManager.ConnectionStrings["sqlConnectionString_totalOut"].ToString();
        string errorLogPath = System.Environment.CurrentDirectory + "\\errorlog.txt";//错误日志记录路径
        string successLogPath = System.Environment.CurrentDirectory + "\\successlog.txt";//成功日志记录路径
        System.Timers.Timer timer_clear = new System.Timers.Timer();
        System.Timers.Timer compareDelayTimer = new System.Timers.Timer();
        bool reportDelay = false;

        public Form1()
        {
            InitializeComponent();
            out_time = out_time * 1000;//秒
            win_time = win_time * 1000;//秒
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            //创建实时同步的线程
            Thread thread = new Thread(new ThreadStart(synBySeconds));
            thread.Start();
            timer_clear.AutoReset = false;
            timer_clear.Interval = 10000;
            timer_clear.Elapsed += new System.Timers.ElapsedEventHandler(clear_Elapse);
            timer_clear.Start();

            compareDelayTimer.Interval = 10000;
            compareDelayTimer.AutoReset = false;
            compareDelayTimer.Elapsed += new System.Timers.ElapsedEventHandler(compareSQLTime_Elapsed);
            compareDelayTimer.Start();
        }

        #region 实时同步的方法
        void synBySeconds()
        {
            Thread threadout = new Thread(new ThreadStart(syn_dt_user_out_account));
            threadout.Start();
            Thread threadwin = new Thread(new ThreadStart(syn_dt_lottery_winning_record));
            threadwin.Start();
        }

        void syn_dt_user_out_account()
        {
            string tablename = ConfigurationManager.AppSettings["onlyOne_out"].ToString();
            FillMsg1("正在同步...");
            string date_now = "";
            int count = 0;
            bool restart = false;
            int sleep_time = out_time;
            //無窮迴圈
            while (true)
            {
                date_now = System.DateTime.Now.ToString("yyyy-MM-dd");
                if (dt_users_class_verify_cold() == 0)
                {
                    //每日迴圈
                    while (date_now == System.DateTime.Now.ToString("yyyy-MM-dd"))
                    {
                        if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
                        {
                            if (DateTime.Now.Hour == 3 && DateTime.Now.Minute >= 10 && restart == false)
                            {
                                try
                                {
                                    Process.Start("cmd.exe", @"/c net stop SQLSERVERAGENT  & net stop MSSQLSERVER");
                                    Thread.Sleep(30000);
                                    Process.Start("cmd.exe", @"/c net start MSSQLSERVER  & net start SQLSERVERAGENT");
                                    restart = true;
                                }
                                catch (Exception ex)
                                {
                                    FillErrorMsg("重啟報錯:" + ex);
                                    WriteErrorLog("重啟報錯:" + DateTime.Now.ToString(), ex.ToString());
                                }
                            }
                            else if (restart == false)
                            {
                                Thread.Sleep(1000 * 60 * 10);
                            }
                            else if (restart == true)
                            {
                                Thread.Sleep(1000 * 60 * 60 * 3 - 1000 * 60 * 9);
                            }
                            continue;
                        }
                        restart = false;
                        if (dt_users_class_verify() == 0)
                        {
                            count = 0;
                            try
                            {
                                List<SqlParameter> LocalSqlParamter = new List<SqlParameter>(){new SqlParameter("@tableName", tablename) };
                                string querystr = "select lockid from dt_tablelockid where tableName=@tableName";
                                var locktable = SqlDbHelper.GetQuery(querystr, LocalSqlParamter.ToArray(), sql_agentOut);
                                byte[] lockid = StringConvertByte(locktable.Rows[0]["lockid"].ToString());
                                List<string> maxLockid_list = new List<string>();
                                List<SqlParameter> AzureSqlParamter = new List<SqlParameter>();
                                SqlParameter pa_lockid = new SqlParameter("@lockid", SqlDbType.Timestamp);
                                pa_lockid.Value = lockid;
                                AzureSqlParamter.Add(pa_lockid);

                                string str = "select top " + out_count  + " * from " + tablename + " where lockid>@lockid and user_id in(select id from dt_users where flog=0) and ((type in(3, 9, 10) and state = 1) or (type = 3 and state = 3)) order by lockid asc OPTION (RECOMPILE)";
                                var table = SqlDbHelper.GetQuery(str, AzureSqlParamter.ToArray(), (reportDelay == true ? fufuzhuConnStr : reportConnStr));
                                count = table.Rows.Count;
                                if (table.Rows.Count != 0)//有数据时更新到本地数据库中
                                {
                                    for (int i = table.Rows.Count - 1; i >= 0; i--)
                                    {
                                        byte[] a = (byte[])table.Rows[i]["lockid"];
                                        string lockid_list = BitConverter.ToString(a).Replace("-", "");
                                        maxLockid_list.Add(lockid_list);
                                    }
                                    string maxlockid = "0x" + maxLockid_list.Max();
                                    table.Columns.Remove("lockid");
                                    TimeSpan tsA1 = new TimeSpan(DateTime.Now.Ticks);
                                    int resultA = SqlDbHelper.RunInsert(table, maxlockid, "dsp_user_out_proxySum", sql_agentOut);
                                    TimeSpan tsA2 = new TimeSpan(DateTime.Now.Ticks);
                                    TimeSpan tsA = tsA1.Subtract(tsA2).Duration();
                                    string dateDiffA = tsA.Seconds.ToString() + "秒";
                                    FillMsg1("Agent 成功汇总" + count + "条数据,耗时:" + dateDiffA);
                                    WriteLogData("Agent 成功汇总" + count + "条数据,耗时:" + dateDiffA + "    同步汇总时间:" + DateTime.Now.ToString());
                                    //一樣的資料送到TotalOut
                                    TimeSpan tsT1 = new TimeSpan(DateTime.Now.Ticks);
                                    int resultT = SqlDbHelper.RunInsert(table, maxlockid, "dsp_user_out_account_synSum", sql_totalOut);
                                    TimeSpan tsT2 = new TimeSpan(DateTime.Now.Ticks);
                                    TimeSpan tsT = tsT1.Subtract(tsT2).Duration();
                                    string dateDiffT = tsT.Seconds.ToString() + "秒";
                                    FillMsg1("Total 成功汇总" + count + "条数据,耗时:" + dateDiffT);
                                    WriteLogData("Total 成功汇总" + count + "条数据,耗时:" + dateDiffT + "    同步汇总时间:" + DateTime.Now.ToString());
                                }
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message != "未将对象引用设置到对象的实例。")
                                {
                                    FillErrorMsg(tablename + ":" + ex);
                                    WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                                }
                            }
                            if (count == out_count)
                            {
                                sleep_time = 2000;
                            }
                            else if (count >= out_count / 3 * 2)
                            {
                                sleep_time = out_time;
                            }
                            else if (count < out_count / 3 * 2 && count >= out_count / 3)
                            {
                                sleep_time = out_time * out_waitX;
                            }
                            else if (count < out_count / 3)
                            {
                                sleep_time = out_time * (out_waitX + 2);
                            }
                            Thread.Sleep(sleep_time);//睡眠时间
                        }
                    }
                }
            }
        }

        void syn_dt_lottery_winning_record()
        {
            FillMsg2("正在同步...");
            string tablename = ConfigurationManager.AppSettings["onlyOne_win"].ToString();
            int count = 0;
            int sleep_time = win_time;
            //無窮迴圈
            while (true)
            {
                if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
                {
                    Thread.Sleep(1000 * 60 * 60 * 3);
                    continue;
                }

                count = 0;
                try
                {
                    List<SqlParameter> LocalSqlParamter = new List<SqlParameter>(){new SqlParameter("@tableName",tablename)};
                    string querystr = "select lockid from dt_tablelockid where tableName=@tablename";
                    var locktable = SqlDbHelper.GetQuery(querystr, LocalSqlParamter.ToArray(), sql_agentOut);
                    byte[] lockid = StringConvertByte(locktable.Rows[0]["lockid"].ToString());
                    List<string> maxLockid_list = new List<string>();
                    List<SqlParameter> AzureSqlParamter = new List<SqlParameter>();
                    SqlParameter pa_lockid = new SqlParameter("@lockid", SqlDbType.Timestamp);
                    pa_lockid.Value = lockid;
                    AzureSqlParamter.Add(pa_lockid);

                    string str = string.Format("select top " + win_count + " id,identityid,user_id,lottery_code,bonus_money,add_time,SourceName,lockid from " + tablename + " where lockid>@lockid and flog=0 order by lockid asc OPTION (RECOMPILE)");
                    var table = SqlDbHelper.GetQuery(str, AzureSqlParamter.ToArray(), (reportDelay == true ? fufuzhuConnStr : reportConnStr));
                    count = table.Rows.Count;
                    if (table.Rows.Count != 0)//有数据时更新到本地数据库中
                    {
                        for (int i = table.Rows.Count - 1; i >= 0; i--)
                        {
                            byte[] a = (byte[])table.Rows[i]["lockid"];
                            string lockid_list = BitConverter.ToString(a).Replace("-", "");
                            maxLockid_list.Add(lockid_list);
                        }
                        string maxlockid = "0x" + maxLockid_list.Max();
                        table.Columns.Remove("lockid");

                        TimeSpan tsA1 = new TimeSpan(DateTime.Now.Ticks);
                        int resultA = SqlDbHelper.RunInsert(table, maxlockid, "dsp_lottery_winning_proxySum_self", sql_agentOut);
                        TimeSpan tsA2 = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan tsA = tsA1.Subtract(tsA2).Duration();
                        string dateDiffA = tsA.Seconds.ToString() + "秒";
                        FillMsg2("Agent 成功汇总" + count + "条,耗时:" + dateDiffA);
                        WriteLogData("Agent 成功汇总" + count + "条数据,耗时:" + dateDiffA + "    同步汇总时间:" + DateTime.Now.ToString());
                        //一樣的資料送到TotalIn
                        TimeSpan tsT1 = new TimeSpan(DateTime.Now.Ticks);
                        int resultT = SqlDbHelper.RunInsert(table, maxlockid, "dsp_lottery_winning_synSum_self", sql_totalIn);
                        TimeSpan tsT2 = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan tsT = tsT1.Subtract(tsT2).Duration();
                        string dateDiffT = tsT.Seconds.ToString() + "秒";
                        FillMsg2("Total 成功汇总" + count + "条,耗时:" + dateDiffT);
                        WriteLogData("Total 成功汇总" + count + "条数据,耗时:" + dateDiffT + "    同步汇总时间:" + DateTime.Now.ToString());
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message != "未将对象引用设置到对象的实例。")
                    {
                        FillErrorMsg(tablename + ":" + ex);
                        WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                    }
                }
                if (count == win_count)
                {
                    sleep_time = 1000;
                }
                else if (count >= win_count / 3 * 2)
                {
                    sleep_time = win_time;
                }
                else if (count < win_count / 3 * 2 && count >= win_count / 3)
                {
                    sleep_time = win_time * win_waitX;
                }
                else if (count < win_count / 3)
                {
                    sleep_time = win_time * (win_waitX + 2);
                }
                Thread.Sleep(sleep_time);//睡眠时间
            }
        }

        /// <summary>
        /// 代理汇总前先判断下层级表的准确性 昨天前
        /// </summary>
        int dt_users_class_verify_cold()
        {
            int result = 0;
            int count = 0;
            int reportcount = 0;
            int azurecount = 0;
            int reportmax = 0;
            int azuremax = 0;
            try
            {
                // int xx = Convert.ToInt32("sss0d5fg4");
                string date_now = System.DateTime.Now.ToString("yyyy-MM-dd");
                //比對每日的數量及最大最小值
                List<SqlParameter> LocalSqlParamter = new List<SqlParameter>()
                {
                    new SqlParameter("@date_now",date_now)
                };
                string str = "select convert(char(10),addtime,121) addtime,count(id) count,isnull(min(id),0) min,isnull(max(id),0) max from dt_users_class where addtime < @date_now group by convert(char(10),addtime,121) order by convert(char(10),addtime,121) OPTION (RECOMPILE)";
                //获取报表中的层级最新的id
                var reporttable = SqlDbHelper.GetQuery(str, LocalSqlParamter.ToArray(), sql_agentOut);
                //获取从库中层级最新的id
                var azurettable = SqlDbHelper.GetQuery(str, LocalSqlParamter.ToArray(), (reportDelay == true ? fufuzhuConnStr : reportConnStr));
                TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
                for (int i = 0; i < azurettable.Rows.Count; i++)
                {
                    string date_str = azurettable.Rows[i]["addtime"].ToString().TrimEnd();
                    string date_str_e = Convert.ToDateTime(azurettable.Rows[i]["addtime"]).AddDays(1).ToString("yyyy-MM-dd");
                    azurecount = Convert.ToInt32(azurettable.Rows[i]["count"]);
                    azuremax = Convert.ToInt32(azurettable.Rows[i]["max"]);

                    if (reporttable.Select("addtime ='" + date_str + "'").Count() > 0)
                    {
                        reportmax = Convert.ToInt32(reporttable.Select("addtime ='" + date_str + "'")[0]["max"]);
                        reportcount = Convert.ToInt32(reporttable.Select("addtime ='" + date_str + "'")[0]["count"]);
                        if (azurecount > reportcount || azuremax > reportmax)//如報表有叢庫當天資料,並且報表數量和maxid都小於叢庫
                        {
                            count = Crue_data(azurecount, reportcount, azuremax, reportmax, date_str, date_str_e);
                        }
                    }
                    else //如果報表沒叢庫當天數據則補數據
                    {
                        count = Crue_data(azurecount, reportcount, azuremax, reportmax, date_str, date_str_e);
                    }
                }
                TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
                TimeSpan ts = ts1.Subtract(ts2).Duration();
                string dateDiff = ts.Seconds.ToString() + "秒";
                FillMsg3("dt_user_class成功同步汇总" + count + "条数据,耗时:" + dateDiff);
                return result ;
            }
            catch (Exception ex)
            {
                FillErrorMsg("dt_users_class_verify_cold:" + ex);
                WriteErrorLog("dt_users_class_verify_cold:" + DateTime.Now.ToString(), ex.ToString() + "  補冷數據");
                return 1;
            }
        }
        /// <summary>
        /// 代理汇总前先判断下层级表的准确性  今天
        /// </summary>
        int dt_users_class_verify()
        {
            int result = 0;
            try
            {
                int count = 0;
                int reportcount = 0;
                int azurecount = 0;
                int reportmax = 0;
                int azuremax = 0;
                int reportmin = 0;
                int azuremin = 0;
                string date_now = System.DateTime.Now.ToString("yyyy-MM-dd");
                List<SqlParameter> LocalSqlParamter = new List<SqlParameter>();
                SqlParameter patime = new SqlParameter("@addtime", SqlDbType.DateTime);
                patime.Value = date_now;
                LocalSqlParamter.Add(patime);
                //int xx = Convert.ToInt32("sss0d5fg4");
                string str = "select count(id) count,isnull(min(id),0) min,isnull(max(id),0) max  from dt_users_class where addtime>=@addtime OPTION (RECOMPILE)";
                //获取报表中的层级最新的id
                var reporttable = SqlDbHelper.GetQuery(str, LocalSqlParamter.ToArray(),sql_agentOut);
                if (reporttable.Rows.Count > 0 && reporttable != null)
                {
                    reportcount = Convert.ToInt32(reporttable.Rows[0]["count"]);
                    reportmax = Convert.ToInt32(reporttable.Rows[0]["max"]);
                    reportmin = Convert.ToInt32(reporttable.Rows[0]["min"]);
                    //获取从库中层级最新的id
                    var azurettable = SqlDbHelper.GetQuery(str, LocalSqlParamter.ToArray(), (reportDelay == true ? fufuzhuConnStr : reportConnStr));
                    azurecount = Convert.ToInt32(azurettable.Rows[0]["count"]);
                    azuremax = Convert.ToInt32(azurettable.Rows[0]["max"]);
                    azuremin = Convert.ToInt32(azurettable.Rows[0]["min"]);
                    if (reportmax >= azuremax && reportcount >= azurecount)//叢庫id<=報表id且叢庫數量<=報表數量表示同步數量相同
                    {

                        return result = 0;
                    }
                    else if (reportcount < azurecount || reportmax < azuremax)//如報表有叢庫當天資料,並且報表數量和maxid都小於叢庫
                    {

                        string date_str = System.DateTime.Now.ToString("yyyy-MM-dd");
                        string date_str_e = System.DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                        //查看叢庫中id等於報表最大id內的資料比數
                        List<SqlParameter> UserClassSqlParamter = new List<SqlParameter>();
                        SqlParameter patime_now = new SqlParameter("@date_now", SqlDbType.DateTime);
                        patime_now.Value = date_now;
                        SqlParameter pamax = new SqlParameter("@reportmax", SqlDbType.Int);
                        pamax.Value = reportmax;
                        UserClassSqlParamter.Add(patime_now);
                        UserClassSqlParamter.Add(pamax);
                        string report_count_str = "select count(id) count  from dt_users_class where id<= @reportmax  and addtime>=@date_now OPTION (RECOMPILE)";
                        var check_report = SqlDbHelper.GetQuery(report_count_str, UserClassSqlParamter.ToArray(), sql_agentOut);
                        int check_reportcount = Convert.ToInt32(check_report.Rows[0]["count"]);
                        var check_azuret = SqlDbHelper.GetQuery(report_count_str, UserClassSqlParamter.ToArray(), (reportDelay == true ? fufuzhuConnStr : reportConnStr));
                        int check_azurecount = Convert.ToInt32(check_azuret.Rows[0]["count"]);
                        // string crue_str = "insert into dt_users_class (id,identityid,father_id,user_id,user_class,isagent,uclass,addtime)values";
                        bool issuccess = false;
                        string str_azure = "";
                        try
                        {
                            //string str_report = "";
                            TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
                            if (check_reportcount >= check_azurecount)//報表比數大於等於叢庫,表示只需要同步新資料
                            {
                                List<SqlParameter> UserClass_SqlParamter = new List<SqlParameter>();
                                SqlParameter padate_s = new SqlParameter("@date_str", SqlDbType.DateTime);
                                padate_s.Value = date_str;
                                UserClass_SqlParamter.Add(padate_s);
                                SqlParameter padate_e = new SqlParameter("@date_str_e", SqlDbType.DateTime);
                                padate_e.Value = date_str_e;
                                UserClass_SqlParamter.Add(padate_e);
                                UserClass_SqlParamter.Add(pamax);

                                str_azure = "select id,identityid,father_id,user_id,user_class,isagent,uclass,convert(varchar,addtime,121) as addtime from dt_users_class where id>@reportmax and addtime >=@date_str and addtime<=@date_str_e OPTION (RECOMPILE)";
                                DataTable dt_azuret = SqlDbHelper.GetQuery(str_azure, UserClass_SqlParamter.ToArray(), (reportDelay == true ? fufuzhuConnStr : reportConnStr));
                                RunSqlBulkCopy(dt_azuret, "dt_users_class", ref issuccess, sql_agentOut);
                                count = dt_azuret.Rows.Count;
                            }
                            else//需要整天重新檢查
                            {
                                count = Crue_data(azurecount, reportcount, azuremax, reportmax, date_str, date_str_e);
                            }
                            TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
                            TimeSpan ts = ts1.Subtract(ts2).Duration();
                            string dateDiff = ts.Seconds.ToString() + "秒";
                            FillMsg3("dt_user_class成功同步汇总" + count + "条数据,耗时:" + dateDiff);
                        }
                        catch (Exception ex)
                        {
                            FillErrorMsg("dt_users_class_verify:" + ex);
                            FillErrorMsg("sql语句:" + str_azure);
                            WriteErrorLog("dt_users_class_verify:" + DateTime.Now.ToString(), ex.ToString() + "\r\n" + "sql语句:" + str_azure + "\r\n" + "變數check_reportcount:" + check_reportcount + "\t" + "check_azurecount:" + check_azurecount);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                if (ex.Message != "未将对象引用设置到对象的实例。")
                {
                    FillErrorMsg("dt_users_class_verify:" + ex);
                    WriteErrorLog("dt_users_class_verify:連線失敗  " + DateTime.Now.ToString(), ex.ToString());
                }
                return 1;
            }
        }
        /// <summary>
        /// 批量插入
        /// </summary>
        private void RunSqlBulkCopy(DataTable dt, string tablename, ref bool isSuccess, string connectString)
        {
            if (dt.Rows.Count != 0)
            {
                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connectString))
                {
                    //if (tablename == "dt_user_out_account")
                    //    tablename = "tempdb.dbo.##dt_user_out_account_temp";
                    sqlBulkCopy.DestinationTableName = tablename;
                    sqlBulkCopy.BulkCopyTimeout = 6000;
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (dt.Columns[i].ColumnName != "lockid")
                            sqlBulkCopy.ColumnMappings.Add(dt.Columns[i].ColumnName, dt.Columns[i].ColumnName);
                    }
                    try
                    {
                        isSuccess = true;
                        sqlBulkCopy.WriteToServer(dt);
                    }
                    catch (Exception ex)
                    {
                        isSuccess = false;
                        FillErrorMsg("批量插入" + tablename + ":" + ex);
                        WriteErrorLog("批量插入" + tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                    }
                }
            }
        }
        /// <summary>
        /// 補數據
        /// </summary>
        private int Crue_data(int azurecount, int reportcount, int azuremax, int reportmax, string date_s_str, string date_e_str)
        {
            //誤差比數
            int diff = azurecount - reportcount;
            int crue = 0;
            while (crue < diff || reportmax < azuremax)
            {
                //string date_s_str = date_s.ToString("yyyy-MM-dd");
                //string date_e_str = date_e.ToString("yyyy-MM-dd");
                List<SqlParameter> UserClass_SqlParamter = new List<SqlParameter>();
                SqlParameter padate_s = new SqlParameter("@date_s_str", SqlDbType.DateTime);
                padate_s.Value = date_s_str;
                UserClass_SqlParamter.Add(padate_s);
                SqlParameter padate_e = new SqlParameter("@date_e_str", SqlDbType.DateTime);
                padate_e.Value = date_e_str;
                UserClass_SqlParamter.Add(padate_e);
                string crue_str = "insert into dt_users_class (id,identityid,father_id,user_id,user_class,isagent,uclass,addtime)values";
                string str_azure = "select id,identityid,father_id,user_id,user_class,isagent,uclass,convert(varchar,addtime,121) as addtime from dt_users_class where addtime >=@date_s_str and addtime<=@date_e_str OPTION (RECOMPILE)";
                string str_report = "select  id,identityid,father_id,user_id,user_class,isagent,uclass,convert(varchar,addtime,121) as addtime from dt_users_class where addtime >=@date_s_str and addtime<=@date_e_str OPTION (RECOMPILE)";
                DataTable dt_azure = SqlDbHelper.GetQuery(str_azure, UserClass_SqlParamter.ToArray(), (reportDelay == true ? fufuzhuConnStr : reportConnStr));
                DataTable dt_report = SqlDbHelper.GetQuery(str_report, UserClass_SqlParamter.ToArray(), sql_agentOut);
                try
                {
                    if (dt_azure.Rows.Count > dt_report.Rows.Count)
                    {
                        for (int r = 0; r < dt_azure.Rows.Count; r++)
                        {
                            string azure_id = dt_azure.Rows[r]["id"].ToString();
                            if (dt_report.Select("id = " + azure_id + "").Count() == 0)
                            {
                                crue++;
                                if (reportmax < Convert.ToInt32(azure_id))
                                {
                                    reportmax = Convert.ToInt32(azure_id);
                                }
                                string identityid = dt_azure.Rows[r]["identityid"].ToString();
                                string father_id = dt_azure.Rows[r]["father_id"].ToString();
                                string user_id = dt_azure.Rows[r]["user_id"].ToString();
                                string user_class = dt_azure.Rows[r]["user_class"].ToString();
                                string isagent = dt_azure.Rows[r]["isagent"].ToString();
                                string uclass = dt_azure.Rows[r]["uclass"].ToString();
                                string addtime = dt_azure.Rows[r]["addtime"].ToString();
                                crue_str += " ('" + azure_id + "','" + identityid + "','" + father_id + "','" + user_id + "','" + user_class + "','" + isagent + "','" + uclass + "','" + addtime + "' ),";
                            }
                        }
                        if (crue > 0)
                        {
                            crue_str = crue_str.Substring(0, crue_str.Length - 1);
                            SqlDbHelper.ExecuteNonQuery(crue_str, sql_agentOut);
                        }
                    }
                    //  date_s = date_s.AddDays(-1);
                    //  date_e = date_e.AddDays(-1);
                }

                catch (Exception ex)
                {
                    FillErrorMsg("Crue_data:" + ex);
                    FillErrorMsg("sql语句:" + crue_str);
                    WriteErrorLog("Crue_data:" + DateTime.Now.ToString(), ex.ToString() + "\r\n" + "sql语句:" + crue_str + "\r\n" + "變數:crue" + crue + "\t" + "azurecount:" + azurecount + "\t" + "reportcount:" + reportcount + "\t" + "azuremax:" + azuremax + "\t" + "reportmax:" + reportmax + "\t" + "date_s_str:" + date_s_str + "\t" + "date_e_str:" + date_e_str);
                }
            }
            return crue;
        }

        private byte[] StringConvertByte(string sqlstring)
        {
            string stringFromSQL = sqlstring;
            List<byte> byteList = new List<byte>();

            string hexPart = stringFromSQL.Substring(2);
            for (int i = 0; i < hexPart.Length / 2; i++)
            {
                string hexNumber = hexPart.Substring(i * 2, 2);
                byteList.Add((byte)Convert.ToInt32(hexNumber, 16));
            }

            byte[] original = byteList.ToArray();
            return original;
        }
        #endregion

        #region richTextBox记录
        private delegate void RichBox1(string msg);
        private void FillMsg1(string msg) 
        {
            if (richTextBox1.InvokeRequired)
            {
                RichBox1 rb = new RichBox1(FillMsg1);
                richTextBox1.Invoke(rb, new object[] { msg });
            }
            else 
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.AppendText(msg);
                    richTextBox1.AppendText("\r\n");
                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.SelectionLength = 0;
                    richTextBox1.Focus();
                }
            }
        }

        private delegate void RichBox2(string msg);
        private void FillMsg2(string msg)
        {
            if (richTextBox2.InvokeRequired)
            {
                RichBox2 rb = new RichBox2(FillMsg2);
                richTextBox2.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox2.IsHandleCreated)
                {
                    richTextBox2.AppendText(msg);
                    richTextBox2.AppendText("\r\n");
                    richTextBox2.SelectionStart = richTextBox2.Text.Length;
                    richTextBox2.SelectionLength = 0;
                    richTextBox2.Focus();
                }
            }
        }

        private delegate void RichBox3(string msg);
        private void FillMsg3(string msg)
        {
            if (richTextBox3.InvokeRequired)
            {
                RichBox3 rb = new RichBox3(FillMsg3);
                richTextBox3.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox3.IsHandleCreated)
                {
                    richTextBox3.AppendText(msg);
                    richTextBox3.AppendText("\r\n");
                    richTextBox3.SelectionStart = richTextBox3.Text.Length;
                    richTextBox3.SelectionLength = 0;
                    richTextBox3.Focus();
                }
            }
        }

        private delegate void RichBoxErr(string msg);
        private void FillErrorMsg(string msg)
        {
            if (errorBox.InvokeRequired)
            {
                RichBoxErr rb = new RichBoxErr(FillErrorMsg);
                errorBox.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (errorBox.IsHandleCreated)
                {
                    errorBox.AppendText(msg);
                    errorBox.AppendText("\r\n");
                    errorBox.SelectionStart = errorBox.Text.Length;
                    errorBox.SelectionLength = 0;
                    errorBox.Focus();
                }
            }
        }
        #endregion

        #region 打印成功日志记录
        private object obj1 = new object();
        public void WriteLogData(string msgex)
        {
            lock (obj1)
            {
                if (!File.Exists(successLogPath))
                {
                    FileStream fs1 = new FileStream(successLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.Write(msgex);
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(successLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.Write(msgex);
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 错误日志记录
        private object obj = new object();
        public void WriteErrorLog(string msgex, string msgsql)
        {
            lock (obj)
            {
                if (!File.Exists(errorLogPath))
                {
                    FileStream fs1 = new FileStream(errorLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.WriteLine(msgex);
                    sw.WriteLine(msgsql);
                    sw.WriteLine();
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(errorLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.WriteLine(msgex);
                    sr.WriteLine(msgsql);
                    sr.WriteLine();
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 清理textbox
        private void clear_Elapse(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (System.DateTime.Now.ToString("mm") == "40")
                    ClearMsg();
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {

            }
            finally
            {
                timer_clear.Start();
            }
        }
        private delegate void RichBoxClear();
        private void ClearMsg()
        {
            if (richTextBox1.InvokeRequired & richTextBox2.InvokeRequired & richTextBox3.InvokeRequired & errorBox.InvokeRequired)
            {
                RichBoxClear rb = new RichBoxClear(ClearMsg);
                richTextBox1.Invoke(rb);
                richTextBox2.Invoke(rb);
                richTextBox3.Invoke(rb);
                errorBox.Invoke(rb);
            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.Clear();
                    richTextBox2.Clear();
                    richTextBox3.Clear();
                    errorBox.Clear();
                }
            }
        }
        #endregion

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Dispose();
            Application.Exit();
            System.Environment.Exit(0);
        }

        private void compareSQLTime_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                string cmd = "select MAX(add_time) as add_time from dt_lottery_orders";
                DataTable tempTable = new DataTable();
                DateTime timeFufuzhu;
                DateTime timeReport;

                tempTable = SqlDbHelper.GetQuery(cmd, fufuzhuConnStr);
                timeFufuzhu = Convert.ToDateTime(tempTable.Rows[0]["add_time"]);
                tempTable = SqlDbHelper.GetQuery(cmd, reportConnStr);
                timeReport = Convert.ToDateTime(tempTable.Rows[0]["add_time"]);

                if (timeFufuzhu.Subtract(timeReport) >= TimeSpan.FromSeconds(60))
                {
                    if (reportDelay == false)
                    {
                        reportDelay = true;
                        WriteErrorLog("--------------------------\r\n報表從庫延遲", DateTime.Now.ToString() + "\r\n--------------------------");
                    }
                }
                else
                {
                    if (reportDelay == true)
                    {
                        WriteErrorLog("--------------------------\r\n報表從庫恢復", DateTime.Now.ToString() + "\r\n--------------------------");
                    }
                    reportDelay = false;
                }
            }
            catch (Exception ex)
            {
                reportDelay = true;
            }
            finally
            {
                compareDelayTimer.Start();
            }
        }
    }
}
